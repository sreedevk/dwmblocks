// Modify this file to change what commands output to your statusbar, and
// recompile using the make command.
static const Block blocks[] = {
    /*Icon*/ /*Command*/ /*Update Interval*/ /*Update Signal*/
    {"", "~/.config/dwmblocks/dnd", 1, 0},
    {"󰕾  ", "~/.config/dwmblocks/audio", 1, 0},
    {"󰘚  ", "~/.config/dwmblocks/memory", 15, 0},
    {"󰍛  ", "~/.config/dwmblocks/cpu-temp", 15, 0},
    {"󰟽  ", "~/.config/dwmblocks/gpu", 15, 0},
    {"", "~/.config/dwmblocks/wireless", 15, 0},
    {"", "~/.config/dwmblocks/localtime", 1, 0},
    {"", "~/.config/dwmblocks/battery", 1, 0},
};

// sets delimeter between status commands. NULL character ('\0') means no
// delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
